/*
resource "aws_s3_bucket" "tf_state_bucket" {
  bucket = "kojitechs.tf.101.state.bucket" #variable & count for each
}

resource "aws_s3_bucket_acl" "tf_state_bucket_acl" {
  bucket = aws_s3_bucket.tf_state_bucket.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "tf_state_bucket_versioning" {
  bucket = aws_s3_bucket.tf_state_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}


resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "terraform-state-lock"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID"
 

  attribute {
    name = "LockID"
    type = "S"
  }

 
  }
  */

  locals {
  bucket_id = aws_s3_bucket.tf_state_bucket.id
}

resource "aws_s3_bucket" "tf_state_bucket" {
    for_each = { for s3_bucket in var.bucket_names: s3_bucket.version => s3}

    #bucket_id = local.bucket.id
    acl = each.value.acl
    versioning = each.value.status
  
}

  