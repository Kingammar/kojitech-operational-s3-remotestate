
variable "s3_bucket" {
    type = string
    default = "kojitechs.tf.101.state.bucket"
  
}

variable "bucket_names" {
    type = list(object({
        bucket_id = string
        acl = string
    version = bool

    }))
    
  
}

